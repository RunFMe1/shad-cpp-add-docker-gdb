#pragma once

#include <set>

template<class T>
class CleverSet {
public:
    bool insert(const T& value) {
        return false;
    }

    bool erase(const T& value) {
        return false;
    }

    bool find(const T& value) const {
        return false;
    }

    size_t size() const {
        return data_.size();
    }
private:
    std::set<T> data_;
};
